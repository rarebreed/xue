# Tree algorithms

This document will go over various tree algorithms.  In particular, I will cover the basics of all trees which include:

- Insertion
- Deletion
- Searching
- Traversing

## Binary ordered tree

This type of tree is the mother of many trees.  It is relatively simple, but this simplicity can cause a serious
problem.  The simplicity is that each node in the tree has a left and right subtree.  The root node of the left subtree
has a value which is less than the value of it's parent, and the node of the right subtree has a value which is greater
than its parent.  Trees are therefore recursive in nature, since the subtree in turn can have it's own subtrees.  If
the left or right subtree is empty, there are several ways this can be marked.  One simple way is to simply not have a
subtree at all (a Nothing).  Another would be to create a specific singleton type, and emptyTree value.  Any node
which has both its left and right subtrees either as an emptyTree or Nothing would be a "leaf" (since that node itself
doesn't have any "branches")

The problem alluded to earlier is that if you insert an already ordered sequence of values into the tree, the tree will
wind up looking like a linked list.  This will make search times linear since to find any item in the tree, you would
have to do the same thing as searching a linked list (ie, walking through potentially every node to find the value).
Other _self-balancing_ trees exist that solve this problem, but due to their complexity, we will tackle them later.

### Traversing a tree

Many problems require traversing a data structure.  For example, checking to see if a value exists in the structure, or
finding where to insert the next value.  For linked lists, this is pretty simple since there's only way to do a traveral
but trees represent more variety with different uses.  There are three kinds of traversals for trees:

- Pre-order: Record as you descend. parent -> left leaf -> right leaf
- In-order: Record once you hit bottom left leaf -> parent -> right leaf
- Post-order: Record once you hit bottom left leaf -> right leaf -> parent

module Trees.RBTree where

import Prelude
import Control.Bind (bind)
import Data.Maybe (Maybe(..))
import Data.List.Lazy.Types

data Color = Red | Black


data Tree a b = EmptyTree
              | Tree { color :: Color
                     , left :: Tree a b
                     , right :: Tree a b
                     , key :: a
                     , value :: Maybe b
                     }




class TreeC where
  --insert :: forall a b. Tree a b -> a -> b -> Tree a b
  contains :: forall a b. Ord a => a -> Tree a b -> Boolean


isRed :: forall a b. Tree a b -> Boolean
isRed (Tree {color}) =
  case color of
    Red -> true
    Black -> false
isRed EmptyTree = false


isBlack :: forall a b. Tree a b -> Boolean
isBlack t@(Tree {color}) = not isRed t
isBlack EmptyTree = true

getTreeData (Tree {color, key, value, left, right}) =
  { color: color
  , key: key
  , value: value
  , left: left
  , right: right
  }


-- | Maps over the Tree structure
mapt :: forall a b c
      . (b -> c)
     -> Tree a b
     -> Tree a c
mapt f (Tree {color: c, key: k, value: Just v, left: l, right: r}) =
  Tree { color: c
       , left: mapt f l
       , right: mapt f r
       , key: k
       , value: f v }
mapt f (Tree {color: c, key: k, value: Nothing, left: l, right: r}) =
  Tree { color: c
       , left: mapt f l
       , right: mapt f r
       , key: k
       , value: Nothing }
mapt f EmptyTree = EmptyTree


-- | The beauty of haskell dialects :)  In order traversal algorithm
traverse :: forall a b
          . Tree a b
         -> List (Tree a b)
traverse EmptyTree = nil
traverse root@(Tree {left: l, right: r}) = traverse l <> (root : traverse r)



insert :: forall a b
        . Ord a
       => Tree a b
       -> a
       -> b
       -> Tree a b
insert root@(Tree {key: k, value: v, left: l, right: r})
  key val | key < k = Tree { color: Black
                           , left: insert l key val
                           , right: r
                           , key: k
                           , value: v }
          | key > k = Tree { color: Black
                           , left: l
                           , right: insert r key val
                           , key: k
                           , value: v }
          | otherwise = root
insert EmptyTree key val = Tree { color: Black
                                , left: EmptyTree
                                , right: EmptyTree
                                , key: key
                                , value: Just val }

et = insert EmptyTree "mark" "hamill"
et1 = insert et "sean" "toner"
et2 = insert et1 "harrison" "ford"
et3 = insert et2 "alec" "guiness"

lt = traverse et3
